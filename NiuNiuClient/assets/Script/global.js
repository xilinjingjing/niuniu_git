/**
 * Create by Jin on 2021/2/4
 **/

 import layerController from "./layerController";
 import dataManager from "./dataManager";
 var config = require("config");

 const global = {
    layerController : layerController.getInstance(),
    dataManager : dataManager.getInstance(),
    config : config,
 }

 export default global;