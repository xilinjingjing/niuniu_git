/**
 * Create by Jin on 2021/2/20
 **/

import dataManager from "./dataManager";

 class socket{
    constructor(){
        // this.ws = new WebSocket(address);
        this.ws = null;
        this.ENetState = {
            NET_WAIT_FOR_CONNECT: 0x0400,
            NET_CONNECT: 0x0401,
            NET_DISCONNECT: 0x0402,
            NET_DISCONNECTING: 0x0403,
        };
        this.m_kNetState = this.ENetState.NET_DISCONNECT;
    }

    //链接
    DoConnet(url){
        console.log("Websocket connect to: " + url);
        var self = this;
        this.ws = new WebSocket(url);

        this.m_kNetState = this.ENetState.NET_WAIT_FOR_CONNECT;
        this.ws.onopen = function(result){
            if(self.ws){
                self.m_kNetState = self.ENetState.NET_CONNECT;
            }else{
                console.log('self.ws error',self.ws);
            }
        }

        this.ws.onmessage = this.onMessage;

        this.ws.onclose = function(result){
            console.log("on close: ", result);
            self.m_kNetState = self.ENetState.NET_DISCONNECT;
        }

        this.ws.onerror = function(result){
            console.log("on error: ", result);
            self.m_kNetState = self.ENetState.NET_DISCONNECT;
        }

    }

    onMessage(result){
        // console.log("on message: ", result);
        var data = JSON.parse(result.data);
        console.log("on message: ", data);
        if(data){
            dataManager.getInstance().response(data);
        }
    }

    sendMassage(obj){
        if(this.m_kNetState !== this.ENetState.NET_CONNECT){
            return;
        }
        console.log("webSocket obj: ", obj.id);
        let objJSON = JSON.stringify(obj);

        if(this.ws){
            this.ws.send(objJSON);
        }
    }

 }

 export default socket;

 /*信息数据结构
 obj = {
     id:abc,
     key:加密方法(md5key + id)
     param:{
         a:abc,
         b:bcd,
         .
         .
         .
     }
 }
 
 
 
 */
