/**
 * Create by Jin on 2021/2/20
 * 单例实现
 **/

import socket from "./socket";
import layerController from "./layerController";
var Md5 = require('md5');
var config = require("config");

class dataManager{
    
    constructor(){
        this.socket = new socket();
        this.layerController = layerController.getInstance();
    }

    //request 请求：向服务端发送信息

    //登录 用
    login(ID){
        console.log("ID： ", ID);
        let informationDate = ID;
        //id是本地存储的数字
        let data ={
                id: config.login,
                key : this.encrypt(config.login),
                param: {
                    phone_id: informationDate.phone,
                    passWord: informationDate.password,
                    auth: informationDate.auth
                }
            }
            this.socket.sendMassage(data);
    }

    //response 响应：接收服务器端信息
    response(result){
        // let message = this.socket.message();
        console.log('result: ', result);
        let id = result.id;
        if(!this.auth(id)){
            console.log("无法识别");
            return
        }
        switch(id){
            case 'login'://状态：1.successful：成功 2.failure: 失败
                console.log('login', result);
                if(result.param.isSuccessful){
                    this.layerController.createRole();
                }else{
                    //提示：出现相同号码
                    console.log("提示：出现相同号码 ");
                }
                break;
            default:
                console.log("default: ", message);
                break;
        }
    }

    //authentication身份验证
    auth(id){
        let contrastID = this.encrypt(id);
        
        if(contrastID === key){
            return true
        }
        return false
    }

    //TODO 加密
    encrypt(id){
        let contrastID = null;
        if(id === 'login'){
            contrastID = Md5.b64MD5(config.Md5key + id);
        }else{
            contrastID = Md5.hexMD5(config.Md5key + id);
        }
        return contrastID
    }
}

dataManager.m_Instance = null;
dataManager.getInstance = () =>{
    if(dataManager.m_Instance === null){
        dataManager.m_Instance = new dataManager();

    }
    return dataManager.m_Instance;
}


export default dataManager;