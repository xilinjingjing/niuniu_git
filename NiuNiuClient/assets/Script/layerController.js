/**
 * Create by Jin on 2021/2/4
 * layer跳转
 **/

class layerController{

    constructor(){
        this._MainLayer = undefined;
    }

    //mainLayer,然后加载第一个场景
    main(target){
        this._MainLayer = target;
        this._MainLayer.emit("loading");
    }

    login(){
        this._MainLayer.emit("login");
    }

    createRole(){
        this._MainLayer.emit("createRole");
    }

    hall(){
        this._MainLayer.emit("hall");
    }

    createRoom(){
        this._MainLayer.emit("createRoom");
    }

    joinRoom(){
        this._MainLayer.emit("joinRoom");
    }

    record(){
        this._MainLayer.emit("record");
    }

    setting(){
        this._MainLayer.emit("setting");
    }

    share(){
        this._MainLayer.emit("share");
    }

    table(){
        this._MainLayer.emit("table");
    }
}

layerController.m_Instance = null;
layerController.getInstance = () =>{
    if(layerController.m_Instance === null){
        layerController.m_Instance = new layerController();

    }
    return layerController.m_Instance;
}

export default layerController;