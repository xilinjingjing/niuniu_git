/**
 * Create by Jin on 2021/2/4
 **/

import global from "../global";

var layerState ={
    register: 1,
    login: 2
}
cc.Class({
    extends: cc.Component,

    properties: {
        changeBtn:cc.Button,
        RuleToggle:cc.Toggle,
        ruleContentBtn:cc.Button,
        EB_Phone: cc.EditBox,
        EB_Password: cc.EditBox,
        EB_AuthCode: cc.EditBox,
        btn_AuthCode: cc.Button,

        
    },

    onLoad () {
        // 身份信息
        this.ID = {
            phone:null,
            password : null,
            auth : null
        }

        //条款复选框
        this.RuleToggle.node.on('toggle', this.RuleToggleCallback, this);

        //手机号输入框
        this.EB_Phone.node.on('editing-did-began', this.editBeganCallback, this);
        this.EB_Phone.node.on('editing-did-ended', this.editEndedCallback, this);
        this.EB_Phone.node.on('text-changed', this.textChangedCallback, this);
        this.EB_Phone.node.on('editing-return', this.editReturnCallback, this);
        
        //密码输入框
        this.EB_Password.node.on('editing-did-began', this.PWEditBeganCallback, this);
        this.EB_Password.node.on('editing-did-ended', this.PWEditEndedCallback, this);
        this.EB_Password.node.on('text-changed', this.PWTextChangedCallback, this);
        this.EB_Password.node.on('editing-return', this.PWEditReturnCallback, this);

        //验证码输入框
        this.EB_AuthCode.node.on('editing-did-began', this.ACEditBeganCallback, this);
        this.EB_AuthCode.node.on('editing-did-ended', this.ACEditEndedCallback, this);
        this.EB_AuthCode.node.on('text-changed', this.ACTextChangedCallback, this);
        this.EB_AuthCode.node.on('editing-return', this.ACEditReturnCallback, this);
    },

    start () {

    },

    //TODO  phone password Auth all right,可以登录
    changeLayer(){
        console.log("this.ID: ", this.ID);
        for(let idx in this.ID){
            if(this.ID[idx] == null && this.ID[idx] == undefined){
                console.log("信息填写完整方能进入");
                return
            }
        }
        global.dataManager.login(this.ID);
    },

    RuleToggleCallback(toggle){
        if(toggle.isChecked){
            //同意条约
            this.changeBtn.onEnable();
        }else{
            this.changeBtn.onDisable();
        }
    },

    //phone
    //editing-did-began
    editBeganCallback: function (editbox) {
        console.log("editing-did-began");
     },
    //editing-did-ended
    editEndedCallback: function (editbox) {
        console.log("editing-did-ended");
        console.log("textLabel: ", editbox.string);
        //用正则表达式验证（两种方式：1.严谨点 2.宽泛点） ^1[3|4|5|7|8][0-9]{9}$   ^1[0-9]{10}$
        let phone_RE = /^1[3|4|5|7|8][0-9]{9}$/;
        if(phone_RE.test(Number(editbox.string))){
        //TODO 发送手机号给服务端 判断并用正则表达式判断
            this.ID.phone = editbox.string;
        }else{
            this.ID.phone = null;
            console.log("弹出错误提示");
        }
     },
    //text-changed
    textChangedCallback: function (editbox) {
        console.log("text-changed");
     },
    //editing-return
    editReturnCallback: function (editbox) {

     },


    //password
    //editing-did-began
    PWEditBeganCallback: function (editbox) {
        console.log("editing-did-began-1");
     },
    //editing-did-ended
    PWEditEndedCallback: function (editbox) {
        console.log("editing-did-ended-1");
        //TODO 密码格式验证 密码中必须包含字母（不区分大小写）、数字、特称字符，至少8个字符，最多30个字符；
        let pwdRegex = new RegExp('(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}');
        if(pwdRegex.test(editbox.string)){
            this.ID.password = editbox.string;
        }else{
            this.ID.password = null;
            console.log("您的密码复杂度太低（密码中必须包含字母、数字、特殊字符），请及时修改密码！");
        }
     },
    //text-changed
    PWTextChangedCallback: function (editbox) {
        console.log("text-changed-1");
     },
    //editing-return
    PWEditReturnCallback: function (editbox) {
        console.log("editing-return-1, textLabel: ", editbox.string.length);

     },


    //AuthCode
    ACEditBeganCallback: function (editbox) {
        console.log("editing-did-began-1");
     },
    //editing-did-ended
    ACEditEndedCallback: function (editbox) {
        console.log("editing-did-ended-1");
        let AuthCode_RE = /^[0-9]{6}$/;
        if(AuthCode_RE.test(Number(editbox.string))){
            console.log("this.ID.auth: ", editbox.string);
            this.ID.auth = editbox.string;
        }else{
            this.ID.auth = null;
            console.log("验证码错误，请您重新填写");
        }
     },
    //text-changed
    ACTextChangedCallback: function (editbox) {
        console.log("text-changed-1");
     },
    //editing-return
    ACEditReturnCallback: function (editbox) {
        console.log("editing-return-1, textLabel: ", editbox.string.length);

     },

    ruleContent(){
        //h5页面
        console.log("ruleContent");
    },

    //
    update (dt) {

    },
});
