
/**
 * Create by Jin on 2021/2/4
 **/
import global from "../global";
cc.Class({
    extends: cc.Component,

    properties: {
        changeLayerBtn:cc.Button,
        closeLayerBtn:cc.Button,
    },

    onLoad () {},

    start () {

    },

    changeLayer(){
        global.layerController.table();
    },

    closeLayer(){
        this.node.destroy();
    },
    
    update (dt) {},
});
