
/**
 * Create by Jin on 2021/2/4
 **/

import global from "../global";
cc.Class({
    extends: cc.Component,

    properties: {
        createRoombtn:cc.Button,
        joinRoombtn:cc.Button,
        recordbtn:cc.Button,
        settingbtn:cc.Button,
        sharebtn:cc.Button
    },

    onLoad () {

    },

    start () {

    },
    setCreateRoomLayer(){
        global.layerController.createRoom();
    },
    setJoinRoomLayer(){
        global.layerController.joinRoom();
    },
    setRecordLayer(){
        global.layerController.record();
    },
    setSettingLayer(){
        global.layerController.setting();
    },

    setShareLayer(){
        global.layerController.share();
    },
    update (dt) {},
});
