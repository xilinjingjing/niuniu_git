/**
 * Create by Jin on 2021/2/4
 **/
import global from "../global";
cc.Class({
    extends: cc.Component,

    properties: {
        closeLayerBtn:cc.Button,
    
    },

    onLoad () {},

    start () {

    },
    closeLayer(){
        this.node.destroy();
    },
    update (dt) {},
});