/**
 * Create by Jin on 2021/2/4
 * 1.作为显示所有layer的面板
 **/

import global from "./global";

cc.Class({
    extends: cc.Component,

    properties: {
        // playerInfo : cc.Prefab,
        // playerInfoNode : null

        loadingLayer:cc.Prefab,
        loginLayer:cc.Prefab,
        createRoleLayer:cc.Prefab,
        hallLayer:cc.Prefab,
        createRoomLayer:cc.Prefab,
        joinRoomLayer:cc.Prefab,
        recordLayer:cc.Prefab,
        settingLayer:cc.Prefab,
        shareLayer:cc.Prefab,
        tableLayer:cc.Prefab,
    },
    onLoad () {
        // // console.log("hello world");
        // let ws = new WebSocket("ws://root@47.103.90.153:3001");
        // ws.onopen = function(result){
        //     console.log("on open ",result);
        //     // ws.send("hello world");
        //     let data ={
        //         type: 'login',
        //         data: {
        //             id: 10000
        //         }
        //     }
        //     ws.send(JSON.stringify(data));
        // }
        // ws.onmessage = function(result){
        //     console.log("on message ",result.data);
        //     var message = JSON.parse(result.data);
        //     if(message.type === 'login-success'){
        //         this.refreshUI(message.data);
        //     }
        // }.bind(this);
        // ws.onerror = function(result){
        //     console.log("on error ",result);
        // }
        // this.creatUI();
        // //远程加载图片
        // var remoteUrl = "http://47.103.90.153:3000/images/playerHand_2.jpg";
        // var hand = null;
        // var self = this;
        // cc.assetManager.loadRemote(remoteUrl, function (err, texture) {
        //     cc.log("err:",err,"texture:",texture);
        //     hand = new cc.SpriteFrame(texture);
        //     self.refreshUI(undefined,hand);
        // });
        
        // var temp_arr = [];
        // var temp_id = 18844865545;
        // var array = [{"phone_id":18844865545},{"phone_id":13298825907}];
        // for(let id of array){
        //     console.log(id.phone_id);
        //     temp_arr.push(id.phone_id);
        // }
        // console.log(temp_arr);
        // for(let id of temp_arr){
        //     if(id === temp_id){
        //         console.log("存在相同");
        //     }
        // }
        
        this._curLayer = null;
        this._curSecondLayer = null;
        this.node.on("loading",()=>{
            console.log("loading");
            let node = cc.instantiate(this.loadingLayer);
            node.parent = this.node;
            this._curLayer = node;
        });

        this.node.on("login",()=>{
        this.changeLayer(this.loginLayer);
        });

        this.node.on("createRole",()=>{
            this.changeLayer(this.createRoleLayer);
        });

        this.node.on("hall",()=>{
            this.changeLayer(this.hallLayer);
        });
        this.node.on("createRoom",()=>{
            this.addLayer(this.createRoomLayer);
        });
        this.node.on("joinRoom",()=>{
            this.addLayer(this.joinRoomLayer);
        });
        this.node.on("record",()=>{
            this.addLayer(this.recordLayer);
        });
        this.node.on("setting",()=>{
            this.addLayer(this.settingLayer);
        });
        this.node.on("share",()=>{
            this.addLayer(this.shareLayer);
        });
        this.node.on("table",()=>{
            this.changeLayer(this.tableLayer);
        });
    },

    //两种layer跳转：1.删去前一个layer，生成新Layer  2.在原来layer叠加新layer(都有自我销毁的功能)
    changeLayer(Prefab){
        let node = cc.instantiate(Prefab);
        node.parent = this.node;
        if(this._curLayer){
            this._curLayer.runAction(cc.sequence(
                cc.fadeOut(0.5),
                cc.callFunc(()=>{
                    this._curLayer.destroy();
                    this._curLayer = node;
                })
            ))
        }else{
            this._curLayer = node;
        }
        
    },

    addLayer(Prefab){
        let node = cc.instantiate(Prefab);
        node.parent = this._curLayer;
        if(this._curSecondLayer){
            this._curSecondLayer.runAction(cc.sequence(
                cc.fadeOut(0.5),
                cc.callFunc(()=>{
                    this._curSecondLayer.destroy();
                    this._curSecondLayer = node;
                })
            ))
        }else{
            this._curSecondLayer = node;
        }
    },
    // creatUI(){
    //     let playerInfo = cc.instantiate(this.playerInfo);
    //     this.playerInfoNode = playerInfo;
    //     playerInfo.setPosition(-530,310);
    //     playerInfo.parent = this.node;
    // },

    // refreshUI(data = undefined, hand = undefined){
    //     // var playerInfo = this.node.getName('playerInfo');
    //     if(this.playerInfoNode){
    //         if(data){
    //             this.playerInfoNode.getComponent('playerInfo').setInfoName(data.nickname);
    //             this.playerInfoNode.getComponent('playerInfo').setID(data.id);
    //         }
    //         if(hand){
    //             this.playerInfoNode.getComponent('playerInfo').setHand(hand);
    //         }

            
    //     }
        
    // },
    start () {
        global.layerController.main(this.node);
        global.dataManager.socket.DoConnet(global.config.url);
    },

    update (dt) {
        //TODO scoket链接
        // if(){

        // }
    },
});
