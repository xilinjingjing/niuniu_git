

cc.Class({
    extends: cc.Component,

    properties: {
        InfoName:cc.Label,
        ID:cc.Label,
        hand:cc.Sprite
    },

    onLoad () {
        
    },

    start () {

    },
    setInfoName(str="1111"){
        this.InfoName.string = str;
    },
    setID(str="1111"){
        this.ID.string = "ID:"+str;
    },
    setHand(spriteFrame=undefined){
        //头像都缩小到恒定大小
        // var invariantRect = new cc.Rect(0, 0, 100, 100);
        if(spriteFrame !== undefined){
            // spriteFrame.setRect(invariantRect);
            this.hand.spriteFrame = spriteFrame;
        }
        
    },

    update (dt) {},
});
