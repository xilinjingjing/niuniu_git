/**
 * Create by Jin on 2021/1/13 0958
 */

//TODO 设计MD5加密
const ws = require("nodejs-websocket");
let DB = require('./db');
let db = new DB();
let websocket = ws.createServer(function(client){
    console.log("new client");
    client.on("text", (result)=>{
        console.log("receive message", result);
        // client.send("ok hello world");
        let message = JSON.parse(result);
        let id = message.id;
        let data = message.param;
        console.log("id:",id);
        console.log("data:",data);
        console.log("param:",JSON.stringify(data));
        switch(id){
            case 'login':
                // console.log('login', message);
                // TODO 0.查询手机号是否存在 1.存手机号 2.通知服务端已登录
                db.getTableCoumn('phone_id')
                .then((result)=>{
                    console.log("result:",result);
                    if(db.isSame(data.phone_id, result)){
                        //存在相同号码，通知号码已存在，重新输入
                        client.send(
                            JSON.stringify({
                                id:'login',
                                param:{
                                    isSuccessful : false
                                }
                            }))
                    }else{
                        //存储号码 insertNewData
                        db.insertNewData('user_info',['phone_id'],[data.phone_id])
                        .then((result)=>{
                            //成功储存
                            if(result){
                                client.send(
                                    JSON.stringify({
                                        id:'login',
                                        param:{
                                            isSuccessful : true
                                        }
                                    }))
                            }else{
                                //重新存储
                            }
                        })
                        .catch((err)=>{
                            console.log("err:",err);
                        })
                    }
                })
                .catch((err)=>{
                    console.log("err:",err);
                });
                break;
            default:
                console.log("default: ", message);
                break;
        }
        

        //读取数据库的消息是个异步操作
        // db.getUserInfo(data.id).then((result)=>{
        //     // console.log("result:",result);
        //     //将数据库数据整理好格式，发送给客户端
        //     client.send(JSON.stringify({
        //         id:'login-success',
        //         param:result[0]
        //     }));
        // }).catch((err)=>{
        //     console.log("err:",err);
        // });
    });
    client.on("close", (result)=>{
        console.log("on close", result);
    });
    client.on("error", (result)=>{
        console.log("on error", result);
    });
});
websocket.listen(3001);//端口号