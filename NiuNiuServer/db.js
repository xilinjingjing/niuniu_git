/**
 * Create by Jin on 2021/1/19 1558
 * 专门处理服务器数据库：
 * 
 * 应用主要语法：
 * 一、DML（数据操作语言）
 * 1.select - 从数据库表中获取数据
 * 2.update - 更新数据库表中数据
 * 3.delete - 从数据库表中删除数据
 * 4.insert into - 从数据库表中插入数据
 * 
 * 
 * 二、DDL(数据定义语言)
 * 1.create database - 创建新数据库
 * 2.alter database - 修改数据库
 * 3.create table - 创建新表
 * 4.alter table - 变更（改变）数据库表
 * 5.drop table - 删除表
 * 6.create index - 创建索引（搜索键）
 * 7.drop index - 删除索引
 */

 const MySql = require('mysql');
 class DB{
     constructor(){
         this.closed = false;
         this.mysql = null;
         this.pool = null;
         //Create mysql instantiation
         let pool = MySql.createPool({
             host:'localhost',
             user:'root',
             password:'xia(2021',
             database:'niuniu'
         });
        
         var self = this;
        pool.getConnection(function(err, connection) {
            if(err){
                console.log("数据库建立连接失败");
                self.closed = true;
            } else {
                console.log("数据库建立连接成功");
                self.closed = false;
                self.mysql = connection;
                console.log("pool._allConnections.length:", pool._allConnections.length);
            }
        });
        this.pool = pool;
     }
     
     //查询 个人所有信息
     getUserInfo(id){
         if(this.closed){
            return;
         }
         return new Promise((resole, reject)=>{
            this.mysql.query('select * from user_info where id ='+ id, (err, result)=>{
                if(err){
                    console.log("err:", err);
                    //当返回错误时拒绝
                    reject(err);
                }else{
                    console.log("get user info:", JSON.stringify(result));
                    //顺利拿到消息后，将Promise的回调，返回值
                    resole(result);
                }
            });
         })
     }

     //查询 表中一列所有信息
     getTableCoumn(key){
        if(this.closed){
            return;
         }
         return new Promise((resole, reject)=>{
             console.log("getTableCoumn");
            this.mysql.query('select ' + key + ' from user_info', (err, result)=>{
                if(err){
                    console.log("err:", err);
                    //当返回错误时拒绝
                    reject(err);
                }else{
                    console.log("get phone_id info:", result);
                    console.log("get phone_id info:", JSON.stringify(result));
                    let temp_arr = [];
                    for(let id of result){
                        for(let i in id){
                            temp_arr.push(id[i]);
                        }
                    }

                    //顺利拿到消息后，将Promise的回调，返回值
                    resole(temp_arr);
                }
            });
         })


     }
     


    insertNewData(tableName, key ,values){//1.表 2.键 3.键值
        if(key.length > 1){
            let temp_key = key[0];
            for(let i = 1; i < key.length; i++){  
                temp_key = temp_key + ',' + key[i];
            }
            key = temp_key;
        }else{
            key = key[0];
        }

        if(key.length > 1){
            let temp_values = values[0];
            for(let i = 1; i < values.length; i++){  
                temp_values = temp_values + ',' + values[i];
            }
            values = temp_values;
        }else{
            values = values[0];
        }
        
        return new Promise((resole, reject)=>{
            console.log("insertNewData" + 'INSERT INTO ' + tableName +' (' + key + ') VALUES (' + values + ')');//user_info  phone_id 
           this.mysql.query('INSERT INTO ' + tableName +' (' + key + ') VALUES (' + values + ')', (err, result)=>{
               if(err){
                   console.log("err:", err);
                   reject(err);
               }else{
                    console.log("result:", result);
                   resole(true);
               }
           });
        })
    }

     
     isSame(temp_id, arr){
         console.log("temp_id:",temp_id,"arr:", arr);
        for(let id of arr){
            console.log("id:",id);
            if(id === temp_id){
                console.log("存在相同");
                return true;
            }
        }
        return false;
     }

     createParameters(...param){

     }
 }
 module.exports = DB;

